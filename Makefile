SRC = solution.c
PROGRAM = project

CC = gcc
CFLAGS = -Wall -ansi -O0 -g

all: 

stage0:	
	$(CC) $(CFLAGS) -DSTAGE0 -o $(PROGRAM) $(SRC)

stage1:
	$(CC) $(CFLAGS) -DSTAGE1 -o $(PROGRAM) $(SRC)

stage2:
	$(CC) $(CFLAGS) -DSTAGE2 -o $(PROGRAM) $(SRC)

stage3:
	$(CC) $(CFLAGS) -DSTAGE3 -o $(PROGRAM) $(SRC)

clean:
	rm -f $(PROGRAM)
	rm -f cscope.out

tags:
	cscope -b

.PHONY: all stage0 stage1 stage2 clean clobber tags
