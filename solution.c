/**
 *  Solution to Project 1 for COMP10002 Foundations of Algorithms.
 *
 *  By Matt Signorini <msignorini@unimelb.edu.au>
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include <ctype.h>

/**
 *  We will use conditional compilation to only eable output for certain
 *  stages. However, if stage 1 is enabled, so must be stage 0, and for
 *  stage 2, both stages 0 and 1, and so on.
 */
#ifdef STAGE3
#define STAGE2
#endif

#ifdef STAGE2
#define STAGE1
#endif

#ifdef STAGE1
#define STAGE0
#endif

/** string constants used for formatting output to look pretty. */
#define NEW_SECTION "==================================================\n"
#define SEPARATOR   "---\n"

/** lines contain at most 1,000 chars. */
#define MAX_LINE_LEN            1000

/** The number of highest scoring lines to be kept. */
#define MAX_RESULTS        10

/** 
 *  Each line will be broken into an array of words, or tokens. Each word
 *  will be null terminated, and so a string in its own right, but all of
 *  the words will be stored in a single char array (with multiple nulls to
 *  separate the words).
 */
typedef struct
{
    char *buffer;
    char *original_text;

    int line_number;

    /** this array gives the index into buffer of the first char of each
     *  word. */
    int *start_indices;
    int num_words;
}
tokens_t;

/**
 *  This struct provides a neater container for the queries, and will use
 *  argv as the underlying array.
 */
typedef struct
{
    int count;
    char **terms;
}
queries_t;

/**
 *  We want to retain a list of a certain number of lines with the highest
 *  score, which constitutes our search results once we have processed all
 *  of the text to be searched.
 */
typedef struct
{
    tokens_t *list [MAX_RESULTS];
    double scores [MAX_RESULTS];
    int num_results;
}
results_list_t;

/** definitions for a bool type. */
typedef int bool;

#define false           (1 != 1)
#define true            (0 != 1)


void print_query_terms (queries_t *queries);
tokens_t * read_line (char *buffer, int limit, int line_num);
void free_tokens (tokens_t *garbage);
tokens_t * parse_tokens (char *buffer, int line_num);
unsigned int count_words (char *line);
double get_simple_score (tokens_t *tokens, queries_t *queries);
void update_results (results_list_t *results, tokens_t *tokens, double score);
void print_results (results_list_t *results);
void insert_match (results_list_t *results);
void * checked_malloc (size_t bytes);


/**
 *  Project 1 for COMP10002. This consists of a program that implements some
 *  simple string searching algorithms.
 */
    int
main (int argc, char **argv)
{
    int line = 0;
    char buffer [MAX_LINE_LEN];
    queries_t queries;
    tokens_t *tokens;
    results_list_t results;
    double score;

#ifdef STAGE0
    queries.count = argc - 1;
    queries.terms = argv + 1;

    printf (NEW_SECTION);
    print_query_terms (&queries);
#endif

    results.num_results = 0;

    /** Step through the input provided on stdin, line by line. */
    while ((tokens = read_line (buffer, MAX_LINE_LEN, line)) != NULL)
    {
#ifdef STAGE1
        /** print out the number of words on each line. For the purposes of
         *  the project, words consist only of alphanumeric chars.
         *  Punctuation chars are counted as white space.
         */
        printf ("%s\n", tokens->original_text);
        printf ("S1: line %8d, words = %4d\n", line, tokens->num_words);
#endif /** STAGE1 */

#ifdef STAGE2
        /** we will keep the queries in argc and argv for the sake of
         *  simplicity, but note that we have to make the queries array
         *  start at argv[1] since argv[0] is the name of the program,
         *  which is clearly not one of the query terms. */
        score = get_simple_score (tokens, &queries);
        printf ("S2: line %8d, score = %8.3f\n", line, score);
#endif /** STAGE2 */

#ifdef STAGE3
        update_results (&results, tokens, score);
#endif 

        printf (SEPARATOR);
        line += 1;
    }

#ifdef STAGE3
    print_results (&results);
#endif

    return 0;
}

/**
 *  Print out the query terms for stage 0.
 */
    void
print_query_terms (queries_t *queries)
{
    int i;

    /** Print out all the query terms. This simply calls for us to print out
     *  all the command line params, but not the program binary path.
     */
    printf ("S0: terms = ");

    for (i = 0; i < queries->count; i ++)
        printf ("%s ", queries->terms [i]);

    printf ("\n");

    printf (NEW_SECTION);
}

/**
 *  Rudimentary text simplicity function. Search for words in the corpus,
 *  the line we have read from stdin, and find any words that are absolute
 *  matches for one of the queries. We will then add the length of the
 *  matching word to the score.
 */
    double
get_simple_score (tokens_t *tokens, queries_t *queries)
{
    double score = 0;
    int word_start, i, q;

    /** step through each token, and compare it with each query. */
    for (i = 0; i < tokens->num_words; i ++)
    {
        for (q = 0; q < queries->count; q ++)
        {
            word_start = tokens->start_indices [i];

            if (strcasecmp (&(tokens->buffer [word_start]), 
                  queries->terms [q]) == 0)
            {
                score += (double) strlen (queries->terms [q]);
            }
        }
    }

    return score;
}

/**
 *  Checks if a new line needs to be added into the list of best matches.
 *  If so, the given tokens_t struct will be inserted into the list into
 *  the correct order. If not, the list will be unchanged and the given
 *  tokens_t struct will be deallocated by calling free_tokens().
 *
 *  In the event that the best match list becomes full, inserting a new
 *  match will result in one of the existing matches being evicted from
 *  the list, since it will no longer have one of the best num scores.
 *  In this case, the evicted struct will be deallocated.
 */
    void
update_results (results_list_t *results, tokens_t *tokens, double score)
{
    int num_results = results->num_results;

    /** lines with score 0 are not considered to be matches. */
    if (score == 0)
    {
        free_tokens (tokens);
        return;
    }

    /** If there is space in the best matches list, this item must be
     *  inserted. */
    if (num_results < MAX_RESULTS)
    {
        results->list [num_results] = tokens;
        results->scores [num_results] = score;
        results->num_results ++;
        insert_match (results);
    }
    else if (score > results->scores [num_results - 1])
    {
        /** evict the lowest scoring best match and replace it with our new
         *  match. */
        free_tokens (results->list [num_results - 1]);
        results->list [num_results - 1] = tokens;
        results->scores [num_results - 1] = score;
        insert_match (results);
    }
    else
    {
        /** the new match doesn't have a high enough score to make it onto
         *  the best matches list. Deallocate it. */
        free_tokens (tokens);
    }
}

/**
 *  Shuffles the tokens_t at the end of the match list up the list until
 *  the list is in the correct order. This is the same kind of operation
 *  as used in insertion sort, hence the choice of name for this function.
 */
    void
insert_match (results_list_t *results)
{
    int current_pos = results->num_results - 1;
    double temp_score;
    tokens_t *temp_match;

    while ((current_pos > 0) && (results->scores [current_pos - 1] < 
          results->scores [current_pos]))
    {
        /** The item being inserted has a better score than the next item
         *  up the list. Swap them, and also their scores. */
        temp_score = results->scores [current_pos];
        temp_match = results->list [current_pos];

        results->scores [current_pos] = results->scores [current_pos - 1];
        results->list [current_pos] = results->list [current_pos - 1];

        results->scores [current_pos - 1] = temp_score;
        results->list [current_pos - 1] = temp_match;

        current_pos --;
    }
}

/**
 *  Print out the list of best matching lines.
 */
    void
print_results (results_list_t *results)
{
    int i;

    printf (NEW_SECTION);

    for (i = 0; i < results->num_results; i ++)
    {
        printf ("S3: line %8d, score = %8.3lf\n", 
          results->list [i]->line_number, results->scores [i]);
        printf ("%s\n", results->list [i]->original_text);
        printf (SEPARATOR);
    }
}

/**
 *  Reads a single line of text of length at most limit chars into buffer,
 *  then parses it into a collection of alphanumeric words.
 *
 *  Return value is a pointer to a tokens_t struct containing the words, or
 *  null if there are no more chars to read from stdin. Note that this 
 *  struct is allocated using malloc(), so remember to call free().
 */
    tokens_t *
read_line (char *buffer, int limit, int line_num)
{
    int chars_read = 0, next_char;
    char *buffer_pos = buffer;

    while ((next_char = getchar ()) != '\n')
    {
        if (next_char == EOF)
            break;

        /** only add chars to the buffer if there is still sufficient
         *  space. */
        if (chars_read < limit - 1)
            *buffer_pos ++ = (char) next_char;

        chars_read ++;
    }

    /** append a null byte to the end of the buffer. */
    *buffer_pos = '\0';

    /** If we have reached the end of stdin, return null. */
    if ((chars_read == 0) && (next_char == EOF))
        return NULL;

    return parse_tokens (buffer, line_num);
}

/**
 *  Releases memory allocated to a tokens_t struct. This includes memory
 *  allocated for the two array fields.
 */
    void
free_tokens (tokens_t *garbage)
{
    /** free the internal fields first. */
    free (garbage->buffer);
    free (garbage->start_indices);

    free (garbage);
}

/**
 *  Takes a null terminated string and returns a pointer to a tokens_t
 *  struct containing all the words (sequences of alphanumeric chars).
 *
 *  The struct, and internal array fields, are allocated using malloc(),
 *  when they are no longer needed, remember to call free_tokens().
 */
    tokens_t *
parse_tokens (char *buffer, int line_num)
{
    size_t line_length = sizeof (char) * strlen (buffer) + 1;

    tokens_t *tokens = (tokens_t *) checked_malloc (sizeof (tokens_t));
    tokens->buffer = (char *) checked_malloc (line_length);
    tokens->original_text = (char *) checked_malloc (line_length);
    tokens->line_number = line_num;
    tokens->num_words = count_words (buffer);
    tokens->start_indices = (int *) checked_malloc (sizeof (int) * 
      tokens->num_words);

    int current_index, word_count = 0;
    bool inaword = false;

    /** copy the line from stdin into the tokens struct. Note that we can
     *  safely use strcpy here, since we have allocated the length of the
     *  string we are copying. */
    strcpy (tokens->buffer, buffer);
    strcpy (tokens->original_text, buffer);

    /** initialise the array of tokens by replacing the first non
     *  alphanumeric char after a word with a null byte. */
    for (current_index = 0; tokens->buffer [current_index] != '\0';
      current_index ++)
    {
        if (isalnum ((int) tokens->buffer [current_index]))
        {
            if (!inaword)
            {
                /** Remember where the start of each word is. */
                tokens->start_indices [word_count] = current_index;
                word_count ++;
            }

            inaword = true;
        }
        else
        {
            inaword = false;
            tokens->buffer [current_index] = '\0';
        }
    }

    return tokens;
}

/** 
 *  Count the number of words in a given string. This function treats any
 *  sequence of one or more alphanumeric chars as a word. Punctuation chars
 *  are treated as a space between two words.
 */
    unsigned int
count_words (char *line)
{
    bool inaword = false;
    int word_count = 0;

    /** Step through all the chars in the line, and count how many times
     *  our inaword flag changes from false to true.
     */
    while (*line != '\0')
    {
        /** is this a word char? If so, and it is the first char, increment
         *  the word count.
         */
        if (isalnum ((int) *line))
        {
            if (!inaword)
                word_count ++;

            inaword = true;
        }
        else
        {
            inaword = false;
        }

        line ++;
    }

    return word_count;
}

/**
 *  Wrapper to malloc that doesn't return null
 */
    void *
checked_malloc (size_t bytes)
{
    void *mem = malloc (bytes);

    if (mem == NULL)
    {
        printf ("Error: malloc returned null.\n");
        exit (1);
    }

    return mem;
}

/** vim: set ts=4 sw=4 et : */
